FROM registry.access.redhat.com/ubi8/openjdk-11
LABEL com.toleon.vendor="Brendan Boyd<bboyd@tolaeon.io>"
LABEL com.toleon.version="0.3.1"
LABEL com.toleon.name="sdf-docker"
LABEL com.toleon.url="tolaeon.io"
LABEL com.toleon.vcs-url="https://gitlab.com/tolaeon/sdf-docker.git"

USER root
RUN microdnf install -y wget git findutils
# add centos repos in order to get expect util
RUN wget http://mirror.centos.org/centos/8-stream/BaseOS/x86_64/os/Packages/centos-gpg-keys-8-2.el8.noarch.rpm
RUN rpm -i centos-gpg*rpm
RUN wget http://mirror.centos.org/centos/8-stream/BaseOS/x86_64/os/Packages/centos-stream-repos-8-2.el8.noarch.rpm
RUN rpm -i centos-stream-repos*rpm
RUN microdnf install expect

WORKDIR /usr/lib/sdf
COPY ./install-sdfcli.sh .
RUN ./install-sdfcli.sh

USER 185

